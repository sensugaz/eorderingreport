<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class PrintController extends Controller
{
    public function cart(Request $request)
    {
        $validator = Validator::make($request->all(), [
//            'customerId' => 'required'
        ]);

        if (!$validator->fails()) {
            $cart = file_get_contents('http://qasorder2.toagroup.com:8010/API/cart?customerId=' . $request->get('customerId'));
            $cartJson = json_decode($cart, true);

            if ($cartJson['result'] == 'SUCCESS') {
                $data['products'] = $cartJson['data']['cartList'];
                $data['boms'] = $cartJson['data']['cartBOMItems'];

                $data['totalQty'] = 0;
                $data['totalAmount'] = 0;

                foreach ($cartJson['data']['cartList'] as $value) {
                    if ($value['isBOM'] == '') {
                        $data['totalQty'] += 1;
                    }

                    $data['totalAmount'] += $value['totalAmount'];

                    foreach ($data['boms'] as $bom) {
                        if ($value['productCode'] == $bom['productRefCode']) {
                            $data['totalAmount'] += $bom['price'] * $value['qty'];
                        }
                    }
                }

                $data['totalQty'] += count($data['boms']);

                $pdf = PDF::loadView('print.cart', $data);

                return $pdf->stream();
            }
        } else {
            return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
        }
    }

    public function orderInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
//            'orderId' => 'required'
        ]);

        $orderId = null;

        if ($request->has('arr')) {
            $data = explode(',', $request->get('arr'));

            $orderId = $data[0];
        } else {
            $orderId = $request->get('orderId');
        }

        if (!$validator->fails()) {
            $orderInfo = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderInfo?orderId=' . $orderId);
            $orderInfoJson = json_decode($orderInfo, true);

            if ($orderInfoJson['result'] == 'SUCCESS') {
                $data['totalQty'] = 0;
                $data['totalAmount'] = 0;
                $data['orderInfo'] = $orderInfoJson['data']['order'];

                $data['products'] = $orderInfoJson['data']['orderDetailList'];
                $data['boms'] = $orderInfoJson['data']['orderBOMItems'];

                foreach ($orderInfoJson['data']['orderDetailList'] as $value) {
                    if ($value['isBOM'] == '') {
                        $data['totalQty'] += 1;
                        $data['totalAmount'] += $value['totalAmount'];
                    }
                }

                if (count($orderInfoJson['data']['orderBOMItems']) > 0) {
                    $data['totalQty'] += count($orderInfoJson['data']['orderBOMItems']);

                    foreach ($orderInfoJson['data']['orderBOMItems'] as $value) {
                        $data['totalAmount'] += $value['amount'];
                    }
                }

                $pdf = PDF::loadView('print.order.invoice', $data);

                return $pdf->stream();
            } else {
                return Response()->json([
                    'result' => 'ERROR',
                    'message' => 'order not found'
                ]);
            }
        } else {
            return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
        }
    }

    public function orderDetail(Request $request) {
        $validator = Validator::make($request->all(), [
            // 'orderId' => 'required',
            // 'saleOrderNumber' => 'required'
        ]);

        $orderId = null;
        $saleOrderNumber = null;

        if ($request->has('arr')) {
            $data = explode(',', $request->get('arr'));
            
            $orderId = $data[0];
            $saleOrderNumber = $data[1];
        } else {
            $orderId = $request->get('orderId');
            $saleOrderNumber = $request->get('saleOrderNumber');
        }

        if (!$validator->fails()) {
            $orderInfo = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderInfo?orderId=' . $orderId);
            $orderInfoJson = json_decode($orderInfo, true);

            $orderPrecessInfo = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderPrecessInfo?saleOrderNumber=' . $saleOrderNumber);
            $orderPrecessInfoJson = json_decode($orderPrecessInfo, true);

            if ($orderInfoJson['result'] == 'SUCCESS' && $orderPrecessInfoJson['result'] == 'SUCCESS') {
                $data['orderInfo'] = $orderInfoJson['data']['order'];
                $data['orderPrecessInfo'] = $orderPrecessInfoJson['data']['orderProcessInfo'];
                $data['salesDocument'] = $request->get('saleOrderNumber');
                $data['products'] = $orderPrecessInfoJson['data']['orderProcessItemList'];
                $data['discounts'] = $orderPrecessInfoJson['data']['orderProcessDiscountList'];

                $pdf = PDF::loadView('print.order.detail', $data);

                return $pdf->stream();
            } else {
                return Response()->json([
                    'result' => 'ERROR',
                    'message' => 'order not found'
                ]);
            }
        } else {
            return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
        }
    }

    public function orderStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'orderId' => 'required',
            // 'saleOrderNumber' => 'required'
        ]);

        $orderId = null;
        $saleOrderNumber = null;

        if ($request->has('arr')) {
            $data = explode(',', $request->get('arr'));
            
            $orderId = $data[0];
            $saleOrderNumber = $data[1];
        } else {
            $orderId = $request->get('orderId');
            $saleOrderNumber = $request->get('saleOrderNumber');
        }

        if (!$validator->fails()) {
            $orderInfo = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderInfo?orderId=' . $orderId);
            $orderInfoJson = json_decode($orderInfo, true);

            $orderPrecessInfo = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderPrecessInfo?saleOrderNumber=' . $saleOrderNumber);
            $orderPrecessInfoJson = json_decode($orderPrecessInfo, true);

            $orderProcessTracking = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderProcessTracking?saleOrderNumber=' . $saleOrderNumber);
            $orderProcessTrackingJson = json_decode($orderProcessTracking, true);

            if ($orderInfoJson['result'] == 'SUCCESS' && $orderPrecessInfoJson['result'] == 'SUCCESS') {
                $data['orderInfo'] = $orderInfoJson['data']['order'];
                $data['orderPrecessInfo'] = $orderPrecessInfoJson['data']['orderProcessInfo'];

                $data['products'] = $orderProcessTrackingJson['data']['orderProcessOrderItemList'];
                $data['targetQty'] = 0;
                $data['billQty'] = 0;
                $data['deliQty'] = 0;
                $data['balaQty'] = 0;
                $data['rejeQty'] = 0;

                foreach ($data['products'] as $value) {
                    $data['targetQty'] += $value['targetQty'];
                    $data['billQty'] += $value['billQty'];
                    $data['deliQty'] += $value['deliQty'];
                    $data['balaQty'] += $value['balaQty'];
                    $data['rejeQty'] += $value['rejeQty'];
                }

                $pdf = PDF::loadView('print.order.status', $data);

                return $pdf->stream();
            } else {
                return Response()->json([
                    'result' => 'ERROR',
                    'message' => 'order not found'
                ]);
            }
        } else {
            return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
        }
    }

    public function orderBilling(Request $request) {
        $validator = Validator::make($request->all(), [
            // 'orderId' => 'required',
            // 'saleOrderNumber' => 'required'
        ]);

        $orderId = null;
        $saleOrderNumber = null;

        if ($request->has('arr')) {
            $data = explode(',', $request->get('arr'));
            
            $orderId = $data[0];
            $saleOrderNumber = $data[1];
        } else {
            $orderId = $request->get('orderId');
            $saleOrderNumber = $request->get('saleOrderNumber');
        }

        if (!$validator->fails()) {
            $orderInfo = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderInfo?orderId=' . $orderId);
            $orderInfoJson = json_decode($orderInfo, true);

            $orderPrecessInfo = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderPrecessInfo?saleOrderNumber=' . $saleOrderNumber);
            $orderPrecessInfoJson = json_decode($orderPrecessInfo, true);

            $orderProcessTracking = file_get_contents('http://qasorder2.toagroup.com:8010/API/OrderProcessTracking?saleOrderNumber=' . $saleOrderNumber);
            $orderProcessTrackingJson = json_decode($orderProcessTracking, true);

            if ($orderInfoJson['result'] == 'SUCCESS' && $orderPrecessInfoJson['result'] == 'SUCCESS' && $orderProcessTrackingJson['result'] == 'SUCCESS') {
                $data['orderInfo'] = $orderInfoJson['data']['order'];
                $data['orderPrecessInfo'] = $orderPrecessInfoJson['data']['orderProcessInfo'];

                $orderProcessOrderItemList = $orderProcessTrackingJson['data']['orderProcessOrderItemList'];
                $orderProcessShipmentList = $orderProcessTrackingJson['data']['orderProcessShipmentList'];

                $itemAll = [];

                $data['haveBill'] = [];
                $data['noBill'] = [];

                foreach ($orderProcessOrderItemList as $key => $value) {
                    array_push($itemAll, [
                        'balaQty' => $value['balaQty'],
                        'billQty' => $value['billQty'],
                        'deliQty' => $value['deliQty'],
                        'freeGoods' => $value['freeGoods'],
                        'itmNumber' => $value['itmNumber'],
                        'material' => $value['material'],
                        'materialDes' => $value['materialDes'],
                        'netwr2' => $value['netwr2'],
                        'rejeQty' => $value['rejeQty'],
                        'salesdocument' => $value['salesdocument'],
                        'targetQty' => $value['targetQty'],
                        'unit' => $value['unit'],

                        'billDate' => $orderProcessShipmentList[$key]['billDate'],
                        'billNo' => $orderProcessShipmentList[$key]['billNo'],
                        'custRecDate' => $orderProcessShipmentList[$key]['custRecDate'],
                        'custRecTime' => $orderProcessShipmentList[$key]['custRecTime'],
                        'driveName' => $orderProcessShipmentList[$key]['driveName'],
                        'foragt' => $orderProcessShipmentList[$key]['foragt'],
                        'license' => $orderProcessShipmentList[$key]['license'],
                        'runno' => $orderProcessShipmentList[$key]['runno'],
                        'shipmentDoc' => $orderProcessShipmentList[$key]['shipmentDoc'],
                        'startDat' => $orderProcessShipmentList[$key]['startDat'],
                        'startTime' => $orderProcessShipmentList[$key]['startTime'],
                        'telDrive' => $orderProcessShipmentList[$key]['telDrive']
                    ]);
                }

                foreach ($itemAll as $value) {
                    $arr = [
                        'balaQty' => $value['balaQty'],
                        'billQty' => $value['billQty'],
                        'deliQty' => $value['deliQty'],
                        'freeGoods' => $value['freeGoods'],
                        'itmNumber' => $value['itmNumber'],
                        'material' => $value['material'],
                        'materialDes' => $value['materialDes'],
                        'netwr2' => $value['netwr2'],
                        'rejeQty' => $value['rejeQty'],
                        'salesdocument' => $value['salesdocument'],
                        'targetQty' => $value['targetQty'],
                        'unit' => $value['unit'],

                        'billDate' => $value['billDate'],
                        'billNo' => $value['billNo'],
                        'custRecDate' => $value['custRecDate'],
                        'custRecTime' => $value['custRecTime'],
                        'driveName' => $value['driveName'],
                        'foragt' => $value['foragt'],
                        'license' => $value['license'],
                        'runno' => $value['runno'],
                        'shipmentDoc' => $value['shipmentDoc'],
                        'startDat' => $value['startDat'],
                        'startTime' => $value['startTime'],
                        'telDrive' => $value['telDrive']
                    ];

                    $arr['billDate'] = date('d/m/Y', strtotime($arr['billDate']));

                    if ($value['billNo'] == '') {
                        array_push($data['noBill'], $arr);
                    } else {
                        array_push($data['haveBill'], $arr);

                        if (($value['targetQty'] - $value['billQty']) > 0) {
                            $arr['billQty'] = $value['targetQty'] - $value['billQty'];

                            array_push($data['noBill'], $arr);
                        }
                    }
                }
                
                $pdf = PDF::loadView('print.order.billing', $data)
                    ->setPaper('a4', 'landscape');

                return $pdf->stream();
            } else {
                return Response()->json([
                    'result' => 'ERROR',
                    'message' => 'order not found'
                ]);
            }
        } else {
            return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
        }
    }
}
