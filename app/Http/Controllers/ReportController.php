<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Excel;

class ReportController extends Controller
{
    public function monthly() {
        $files = array_diff(scandir('E:\Report\SAP\QAS\ZFARRP10\M'), array('.', '..'));

        $data = [];

        foreach ($files as $value) {
            array_push($data, $value);
        }

        return Response()->json([
            'result' => 'SUCCESS',
            'data' => $data
        ]);
    }

    public function quarter() {
        $files = array_diff(scandir('E:\Report\SAP\QAS\ZFARRP10\Q'), array('.', '..'));

        $data = [];
	
        foreach ($files as $value) {
            array_push($data, $value);
        }

        return Response()->json([
            'result' => 'SUCCESS',
            'data' => $data
        ]);
    }
	
	public function salesman() {
		$files = array_diff(scandir('E:\Report\BI\Reports'), array('.', '..'));
		
		$data = [];
	
        foreach ($files as $value) {
            array_push($data, $value);
        }

        return Response()->json([
            'result' => 'SUCCESS',
            'data' => $data
        ]);
	}
	
	public function manager() {
		$files = array_diff(scandir('E:\Report\BI\Reports'), array('.', '..'));
		
		$data = [];
	
        foreach ($files as $value) {
            array_push($data, $value);
        }

        return Response()->json([
            'result' => 'SUCCESS',
            'data' => $data
        ]);
	}

    public function getMonthly(Request $request, $fileName) {
        $validator = Validator::make($request->all(), [
            'userName' => 'required',
            'deptId'   => 'required'
        ]);

        if (!$validator->fails()) {
            $path = 'E:\Report\SAP\QAS\ZFARRP10\M';
            $data = $this->getDataForCommission($path, $fileName, $request->get('deptId'), $request->get('userName'));

            return Response()->json($data);
        } else {
            return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
        }
    }

    public function getQuarter(Request $request, $fileName) {
        $validator = Validator::make($request->all(), [
            'userName' => 'required',
            'deptId'   => 'required'
        ]);

        if (!$validator->fails()) {
            $path = 'E:\Report\SAP\QAS\ZFARRP10\Q';
            $data = $this->getDataForCommission($path, $fileName, $request->get('deptId'), $request->get('userName'));

            return Response()->json($data);
        } else {
            return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
        }
    }
	
	public function getSalesman(Request $request, $fileName) {
		$validator = Validator::make($request->all(), [
            'userName' => 'required',
		]);
		
		if (!$validator->fails()) {
			$path = 'E:\Report\BI\Reports';
			$data = $this->getDataForSalesmanOrManager($path, $fileName, $request->get('userName'));
			
			return Response()->json($data);
		} else {
			return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
		}
	}

	public function getManager(Request $request, $fileName) {
		$validator = Validator::make($request->all(), [
            'userName' => 'required',
		]);
		
		if (!$validator->fails()) {
			$path = 'E:\Report\BI\Reports';
			$data = $this->getDataForSalesmanOrManager($path, $fileName, $request->get('userName'));
			
			return Response()->json($data);
		} else {
			return Response()->json([
                'result' => 'ERROR',
                'message' => $validator->errors()
            ]);
		}
	}
	
    private function getDataForCommission($path, $fileName, $deptId, $userName) {
        $data = [];
        $file = $this->getFile($path, $fileName);

        if ($file != false) {
            $file = explode("\n", $file);

            $userName = strtoupper(str_replace('sa_', '', $userName));

            for ($i = 1; $i < count($file); $i++) {
                $column = preg_split('/\t/', $file[$i]);

                if ($column[0] != '') {
                    if (isset($deptId)) {
                        if ($deptId != 4) {
                            $sales_group = (int)$column[2];

                            if ($deptId == 2 && ($column[2] != $userName)) {
                                continue;
                            } else if ($deptId == 3 && ($column[5] != $userName)) {
                                continue;
                            } else if ($deptId == 5 && ($sales_group < 111 || $sales_group > 118)) {
                                continue;
                            } else if ($deptId == 6 && ($sales_group < 121 || $sales_group > 124)) {
                                continue;
                            } else if ($deptId == 7 && ($sales_group < 131 || $sales_group > 133)) {
                                continue;
                            } else if ($deptId == 8 && ($sales_group < 141 || $sales_group > 143)) {
                                continue;
                            } else if ($deptId == 9 && ($sales_group < 151 || $sales_group > 153)) {
                                continue;
                            }

							$attr = [];
							$attr['period'] = $column[0];
							$attr['commission_type'] = $column[1];
							$attr['sales_group'] = $column[2];
							$attr['employee_code'] = $column[3];
							$attr['employee_name'] = $column[4];
							$attr['area_no'] = $column[5];
							$attr['supervisor_code'] = $column[6];
							$attr['supervisor_name'] = $column[7];
							$attr['product'] = $column[8];
							$attr['net_amount'] = $column[9];
							$attr['target_amount'] = $column[10];
							$attr['percent'] = $column[11];
							$attr['reward'] = $column[12];
							$attr['actual_shop'] = $column[13];
							$attr['target_shop'] = $column[14];
							
                            array_push($data, $attr);
                        }
                    }
                }
            }

            if (count($data)) {
                return [
                    'result' => 'SUCCESS',
                    'data' => $data
                ];
            } else {
                return [
                    'result' => 'ERROR',
                    'message' => 'data not found'
                ];
            }
        } else {
            return [
                'result' => 'ERROR',
                'message' => 'file not found'
            ];
        }
    }
	
	private function getDataForSalesmanOrManager($path, $fileName, $userName) {
		$file = Excel::load($path . '\\' . $fileName)->get();
				
		if (count($file)) {		
			$data = [];
		
			$userName = strtoupper(str_replace('sa_', '', $userName));

			for ($i = 1; $i < 50; $i++) {
				$attr = [];
				$attr['year'] = $file[$i]['year'];
				$attr['month'] = $file[$i]['month'];
				$attr['quarter'] = $file[$i]['quarter'];
				$attr['sales_group'] = $file[$i]['sales_group_key'];
				$attr['sales_group_name'] = $file[$i]['sales_group'];
				$attr['area'] = $file[$i]['customer_area_key'];
				$attr['area_name'] = $file[$i]['customer_area_name'];
				$attr['team'] = $file[$i]['team_sales'];
				$attr['category'] = $file[$i]['category_key'];
				$attr['category_name'] = $file[$i]['category'];
				$attr['target_amount'] = $file[$i]['target_amtthb'];
				$attr['net_amount'] = $file[$i]['net_amtthb'];
				
				if ($attr['year'] != null && $attr['month'] != null && $attr['quarter'] != null) {
					if ($attr['category'] != '#') {
						array_push($data, $attr);
					}
				}
			}
			
			if (count($data)) {
				$output = [];
				
				foreach ($data as $key => $value) {
					$attr = [];
					
					$target_amount = [];
					$target_amount[1] = 0;
					$target_amount[2] = 0;
					$target_amount[3] = 0;
					$target_amount[4] = 0;
					
					$net_amount = [];
					$net_amount[1] = 0;
					$net_amount[2] = 0;
					$net_amount[3] = 0;
					$net_amount[4] = 0;
					
					foreach ($data as $k => $v) {
						//
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month']) {
							$target_amount[1] += $v['target_amount'];
						}
						
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month'] && $v['category'] == '1') {
							$target_amount[2] += $v['target_amount'];
						}
						
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month'] && $v['category'] == '2') {
							$target_amount[3] += $v['target_amount'];
						}
						
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month'] && $v['category'] == '3') {
							$target_amount[4] += $v['target_amount'];
						}
						
						// 
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month']) {
							$net_amount[1] += $v['net_amount'];
						}
						
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month'] && $v['category'] == '1') {
							$net_amount[2] += $v['net_amount'];
						}
						
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month'] && $v['category'] == '2') {
							$net_amount[3] += $v['net_amount'];
						}
						
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['month'] == $value['month'] && $v['category'] == '3') {
							$net_amount[4] += $v['net_amount'];
						}
						/*
						if ($v['area'] == $value['area'] && $v['year'] == $value['year'] && $v['quarter'] == $value['quarter']) {
							$net_amount[4] += $v['net_amount'];
						}
						*/
					}
					
					
					$attr['target_amount'] = $target_amount;
					$attr['net_amount'] = $net_amount;
					$attr['summary']['target_amount'] = array_sum($target_amount);
					$attr['summary']['net_amount'] = array_sum($net_amount);
							
					array_push($output, $attr);
				}
				
				return [
                    'result' => 'SUCCESS',
                    'data' => $output
                ];
            } else {
                return [
                    'result' => 'ERROR',
                    'message' => 'data not found'
                ];
            }
		} else {
			return [
                'result' => 'ERROR',
                'message' => 'file not found'
            ];
		}
	}
	
    private function getFile($path, $fileName) {
        $file = @file_get_contents($path . '\\' . $fileName);
		
        if ($file === false) {
            return false;
        } else {
			return $file;
        }
    }
}
