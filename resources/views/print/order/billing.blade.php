@extends('layout.print')

@section('content')
    <div class="print__container">
        <div class="print__header">
            <div class="print__header__logo">
                <img src="{{ asset('images/toa-logo-print.png') }}">
            </div>
            <div class="print__header__issuer">
                <p>บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จํากัด (มหาชน)</p>
                <p>สำนักงาน และศูนย์อุตสาหกรรม ทีโอเอ บางนา-ตราด 31/2 หมู่ 3</p>
                <p>ถนนบางนา-ตราด ตำบลบางเสาธง อ.บางเสาธง จ.สมุทรปราการ 10570</p>
            </div>
            <div class="print__header__option">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50" align="right" class="text-bold">
							วันที่&nbsp;
                        </td>
                        <td>{{ date('d/m/Y', strtotime($orderPrecessInfo['docDate'])) }}</td>
                    </tr>
                    <tr>
                        <td width="50" align="right" class="text-bold">
							เลขที่&nbsp;
                        </td>
                        <td>{{ $orderPrecessInfo['salesDocument'] }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="print__title">
            Order / Billing Tracking
        </div>
        <div class="print__client">
            <div class="print__client__full">
                <div class="print__client__full__left">
                    <table>
                        <tr>
                            <td class="text-bold" width="60">
								ชื่อร้านค้า
                            </td>
                            <td>{{ substr($orderPrecessInfo['payerNo'], 2) }} {{ $orderPrecessInfo['payerName'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-bold" width="60">
								เลขใบสั่งซื้อ
                            </td>
                            <td>
								{{ $orderPrecessInfo['woNumber'] }}
								<strong> / วันที่ </strong>
								{{ date('d/m/Y', strtotime($orderPrecessInfo['createDate'])) }}
							</td>
                        </tr>
                        <tr>
                            <td class="text-bold" width="60">
								เลขที่เอกสาร
                            </td>
                            <td>
                                {{ $orderPrecessInfo['salesDocument'] }}
                                <strong> / วันที่ </strong>
                                {{ date('d/m/Y', strtotime($orderPrecessInfo['docDate'])) }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="print__client__full__right">
                    <table>
                        <tr>
                            <td class="text-bold" width="60">
								สถานที่จัดส่ง
                            </td>
                            <td>
                                @if ($orderInfo['shipCode'])
                                    {{ $orderInfo['shipCode'] }} : {{ $orderInfo['shipName'] }}
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold" width="60">
								จัดส่งโดย
                            </td>
                            <td>
                                @if ($orderInfo['shipCondition'] != '01')
                                    {{ $orderInfo['transportZone'] }} : {{ $orderInfo['transportZoneDesc'] }}
                                @else
									ลูกค้ารับสินค้าเอง
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="print__order">
            <div class="text-bold" style="font-size: 20px;">
				ยังไม่ออกบิล
            </div>
            <table>
                <thead>
                    <tr>
                        <th width="100" align="center">
							รหัสสินค้า
                        </th>
                        <th align="center">
							ชื่อสินค้า
                        </th>
                        <th width="80" align="center">
							จำนวนสั่งซื้อ
                        </th>
                        <th width="80" align="center">
							จำนวนออกบิล
                        </th>
                        <th width="80" align="center">
							จำนวนเงิน
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($noBill as $value)
                        <tr>
                            <td>
                                <div style="font-weight: {{ $value['material'][0] == 'S' ? 'bold' : 'normal' }}">
                                    {{ $value['material'] }}
                                </div>
                            </td>
                            <td>
                                <div style="font-weight: {{ $value['material'][0] == 'S' ? 'bold' : 'normal' }}">
                                    {{ $value['materialDes'] }}

                                    @if ($value['freeGoods'] && $value['material'][0] != 'S')
                                        <span style="color: red">(ของแแถม)</span>
                                    @endif
                                </div>
                            </td>
                            <td align="center">
                                @if ($value['material'][0] != 'S')
                                    {{ $value['targetQty'] }}
                                @endif
                            </td>
                            <td align="center">
                                @if ($value['material'][0] != 'S')
                                    {{ $value['targetQty'] - $value['billQty'] }}
                                @endif
                            </td>
                            <td align="right">
                                @if ($value['material'][0] != 'S')
                                    {{ number_format((float)$value['netwr2'], 2, '.', ',') }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    @if (count($noBill) == 0)
                        <tr>
                            <td colspan="5" align="center">
								ไม่มีข้อมูล
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="text-bold" style="font-size: 20px;">
				ออกบิล
            </div>
            <table>
                <thead>
                    <tr>
                        <th width="50" align="center">เลขที่บิล</th>
                        <th width="80" align="center">ใบนำส่ง / วันเวลา รถออกจากบริษัท</th>
                        <th width="80" align="center">
                            Fwd Agent / <br> ทะเบียนรถ
                        </th>
                        <th width="80" align="center">
							ชื่อคนขับรถ / <br> เบอร์ติดต่อ
                        </th>
                        <th width="60" align="center">
							วัน - เวลา <br>รับบิล
                        </th>
                        <th align="center">
							ผลิตภัณฑ์
                        </th>
                        <th width="45" align="center">
							จำนวนสั่งซื้อ
                        </th>
                        <th width="60" align="center">
							จำนวนออกบิล
                        </th>
                        <th width="60" align="center">
							จำนวนเงิน
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($haveBill as $value)
                        <tr>
                            <td align="center">
                                {{ $value['billNo'] }} <br>
                                {{ $value['billDate'] }}
                            </td>
                            <td align="center">
                                {{ date('d/m/Y', strtotime($value['startDat'])) }} <br>
                                {{ $value['startTime'] }}
                            </td>
                            <td align="center">
                                {{ $value['foragt'] }} <br>
								{{ $value['license'] }}
                            </td>
                            <td align="center">
                                {{ $value['driveName'] }} <br>
                                {{ $value['telDrive'] }}
                            </td>
                            <td align="center">
                                @if ($value['custRecDate'] != '0000-00-00' && $value['custRecTime'] != '00:00:00')
                                    {{ $value['custRecDate'] }} <br>
                                    {{ $value['custRecTime'] }}
                                @endif
                            </td>
                            <td>
								<div style="font-weight: {{ $value['material'][0] == 'S' ? 'bold' : 'normal' }}">
                                {{ $value['material'] }} <br>
                                {{ $value['materialDes'] }}
									@if ($value['freeGoods'] && $value['material'][0] != 'S')
										<span style="color: red">(ของแแถม)</span>
                                    @endif
								</div>
                            </td>
							<td align="center">
                                @if ($value['material'][0] != 'S')
                                    {{ $value['targetQty'] }}
                                @endif
                            </td>
                            <td align="center">
                                @if ($value['material'][0] != 'S')
                                    {{ $value['billQty'] }}
                                @endif
                            </td>
                            <td align="right">
                                @if ($value['material'][0] != 'S')
                                    {{ number_format((float)$value['netwr2'], 2, '.', ',') }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    @if (count($haveBill) == 0)
                        <tr>
                            <td colspan="9" align="center">
									ไม่มีข้อมูล
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection