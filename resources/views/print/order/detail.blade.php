@extends('layout.print')

@section('content')
    <div class="print__container">
        <div class="print__header">
            <div class="print__header__logo">
                <img src="{{ asset('images/toa-logo-print.png') }}">
            </div>
            <div class="print__header__issuer">
                <p>บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จํากัด (มหาชน)</p>
                <p>สำนักงาน และศูนย์อุตสาหกรรม ทีโอเอ บางนา-ตราด 31/2 หมู่ 3</p>
                <p>ถนนบางนา-ตราด ตำบลบางเสาธง อ.บางเสาธง จ.สมุทรปราการ 10570</p>
            </div>
            <div class="print__header__option">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50" align="right" class="text-bold">
							วันที่&nbsp;
                        </td>
                        <td>{{ date('d/m/Y', strtotime($orderInfo['documentDate'])) }}</td>
                    </tr>
                    <tr>
                        <td width="50" align="right" class="text-bold">
							เลขที่&nbsp;
                        </td>
                        <td>{{ $orderInfo['documentNumber'] }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="print__title">
			รายละเอียดการสั่งซื้อ
        </div>
        <div class="print__client">
            <div class="print__client__left" style="height: 223px">
                <table>
                    <tr>
                        <td class="text-bold" width="60">
							ชื่อร้านค้า
                        </td>
                        <td>{{ $orderInfo['customerCode'] }} {{ $orderInfo['customerName'] }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold" width="60">
							ที่อยู่
                        </td>
                        <td>
                            {{ $orderInfo['address'] }} {{ $orderInfo['street'] }} {{ $orderInfo['subDistrictName'] }}
                            <br> {{ $orderInfo['districtName'] }} {{ $orderInfo['cityName'] }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">อีเมล</td>
                        <td>{{ ($orderInfo['customerEmail'] == '') ? '-' : $orderInfo['customerEmail'] }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">โทรศัพท์</td>
                        <td>{{ ($orderInfo['customerTelNo'] == '') ? '-' : $orderInfo['customerTelNo'] }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">สถานที่ส่ง</td>
                        <td>
                            @if ($orderInfo['shipCode'])
                                {{ $orderInfo['shipCode'] }} : {{ $orderInfo['shipName'] }}
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">ที่อยู่สถานที่ส่ง</td>
                        <td>
                            @if ($orderInfo['shipName'] != '' || $orderInfo['shipName'] != null)
                                {{ $orderInfo['shipHouseNo'] }} {{ $orderInfo['shipAddress'] }} {{ $orderInfo['shipDistrictName'] }}
                                <br> {{ $orderInfo['shipCityName'] }} {{ $orderInfo['shipPostCode'] }}
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
            <div class="print__client__right" style="height: 223px">
                <table>
                    <tr>
                        <td class="text-bold" width="60">
							เลขที่ใบสั่งซื้อ
                        </td>
                        <td>
                            {{ $orderInfo['documentNumber'] }}
                            <strong> / วันที่ {{ date('d/m/Y', strtotime($orderInfo['documentDate'])) }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold" width="60">
                            PO number
                        </td>
                        <td>
                            {{ ($orderInfo['customerPO'] != '') ? $orderInfo['customerPO'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold" width="60">
							เลขที่เอกสาร
                        </td>
                        <td>
                            {{ $orderPrecessInfo['salesDocument'] }}
                            <strong> / วันที่ {{ date('d/m/Y', strtotime($orderInfo['requestDate'])) }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold">
                            Request Date
                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($orderInfo['requestDate'])) }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold">
							การชำระเงิน
                        </td>
                        <td>
                            {{ ($orderInfo['paymentTerm'] == 'CASH') ? 'เงินสด' : 'เครดิต' }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold">
							จัดส่งโดย
                        </td>
                        <td>
                            @if ($orderInfo['shipCondition'] != '01')
                                {{ $orderInfo['transportZone'] }} : {{ $orderInfo['transportZoneDesc'] }}
                            @else
								ลูกค้ารับสินค้าเอง
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="print__order">
            <table>
                <thead>
                    <tr>
                        <th align="center">
							รายการ
                        </th>
                        <th width="30px" align="center">
							จำนวน
                        </th>
                        <th width="35px" align="center">
							หน่วย
                        </th>
                        <th width="50px" align="center">
							ราคา / หน่วย <span class="text-red">*</span>
                        </th>
                        <th width="50px" align="center">
							ส่วนลด
                        </th>
                        <th width="30px" align="center">
							ราคาหลัง<br>หักส่วนลด
                        </th>
                        <th width="50px" align="center">
							จำนวนเงิน
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>
                                <div style="font-weight: {{ $product['material'][0] == 'S' ? 'bold' : 'normal' }}">
                                    {{ $product['material'] }} <br> {{ $product['materialDes'] }}

                                    @if ($product['freeGoods'] && $product['material'][0] != 'S')
                                        <span style="color: red">(ของแแถม)</span>
                                    @endif
                                </div>
                            </td>
                            <td align="center">
                                @if ($product['material'][0] != 'S')
                                    {{ $product['targetQty'] }}
                                @endif
                            </td>
                            <td align="center">
                                @if ($product['material'][0] != 'S')
                                    {{ $product['salesUnit'] }}
                                @endif
                            </td>
                            <td align="right">
                                @if ($product['material'][0] != 'S')
                                    {{ number_format((float)$product['pricePerUnit'], 2, '.', ',') }}
                                @endif
                            </td>
                            <td width="50px" align="center">
                                @if ($product['material'][0] != 'S')
                                    {{ $product['discount'] }}
                                @endif
                            </td>
                            <td align="right">
                                @if ($product['material'][0] != 'S')
                                    {{ number_format((float)$product['netwrPerUnit'], 2, '.', ',') }}
                                @endif
                            </td>
                            <td align="right">
                                @if ($product['material'][0] != 'S')
                                    {{ number_format((float)$product['amount'], 2, '.', ',') }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td align="right" colspan="7">
                            <b>{{ number_format((float)$orderPrecessInfo['sumAmount'], 2, '.', ',') }}</b>
                        </td>
                    </tr>
                    @foreach ($discounts as $discount)
                        <tr>
                            <td align="center" style="border-right-color: #ffffff">
                                @if ($discount['type'] == 'หัก')
                                    <span style="color: red">
                                        {{ $discount['type'] }}
                                    </span>
                                @else 
                                    <span style="color: blue">
                                        {{ $discount['type'] }}
                                    </span>
                                @endif
                            </td>
                            <td align="center" colspan="3" style="border-right-color: #ffffff">
                                {{ $discount['description'] }}
                            </td>
                            <td style="border-right-color: #ffffff"></td>
                            <td style="border-right-color: #ffffff"></td>
                            <td align="right">
                                {{ number_format((float)$discount['kwert'], 2, '.', ',') }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th align="right" colspan="6" style="border-bottom-color: #dddddd; border-right-color: #dddddd;">
							รวมมูลค่าสินค้า :
                        </th>
                        <th align="right" style="border-bottom-color: #dddddd !important;">
                            {{ number_format((float)$orderPrecessInfo['netValue3'], 2, '.', ',') }}
                        </th>
                    </tr>
                    <tr>
                        <th align="right" colspan="6" style="border-bottom-color: #dddddd; border-right-color: #dddddd">
							ภาษีมูลค่าเพิ่มอัตรา 7% :
                        </th>
                        <th align="right" style="border-bottom-color: #dddddd !important;">
                            {{ number_format((float)$orderPrecessInfo['vatAmount'], 2, '.', ',') }}
                        </th>
                    </tr>
                    <tr>
                        <th align="right" colspan="6" style="border-right-color: #dddddd">
							ยอดรวม :
                        </th>
                        <th align="right">
                            {{ number_format((float)($orderPrecessInfo['netValue3'] + $orderPrecessInfo['vatAmount']), 2, '.', ',') }}
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection