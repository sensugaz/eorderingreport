@extends('layout.print')

@section('content')
    <div class="print__container">
        <div class="print__header">
            <div class="print__header__logo">
                <img src="{{ asset('images/toa-logo-print.png') }}">
            </div>
            <div class="print__header__issuer">
                <p>บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จํากัด (มหาชน)</p>
                <p>สำนักงาน และศูนย์อุตสาหกรรม ทีโอเอ บางนา-ตราด 31/2 หมู่ 3</p>
                <p>ถนนบางนา-ตราด ตำบลบางเสาธง อ.บางเสาธง จ.สมุทรปราการ 10570</p>
            </div>
            <div class="print__header__option">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50" align="right" class="text-bold">
                            วันที่&nbsp;
                        </td>
                        <td>{{ date('d/m/Y', strtotime($orderInfo['documentDate'])) }}</td>
                    </tr>
                    <tr>
                        <td width="50" align="right" class="text-bold">
                            เลขที่&nbsp;
                        </td>
                        <td>{{ $orderInfo['documentNumber'] }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="print__title">
            ใบสั่งซื้อ
        </div>
        <div class="print__client">
            <div class="print__client__left" style="height: 223px">
                <table>
                    <tr>
                        <td class="text-bold" width="60">
							 ชื่อร้านค้า
                        </td>
                        <td>{{ $orderInfo['customerCode'] }} {{ $orderInfo['customerName'] }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold" width="60">
							 ที่อยู่
                        </td>
                        <td>
                            {{ $orderInfo['address'] }} {{ $orderInfo['street'] }} {{ $orderInfo['subDistrictName'] }}
                            <br> {{ $orderInfo['districtName'] }} {{ $orderInfo['cityName'] }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">อีเมล</td>
                        <td>{{ ($orderInfo['customerEmail'] == '') ? '-' : $orderInfo['customerEmail'] }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">โทรศัพท์</td>
                        <td>{{ ($orderInfo['customerTelNo'] == '') ? '-' : $orderInfo['customerTelNo'] }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">สถานที่ส่ง</td>
                        <td>
                            @if ($orderInfo['shipCode'])
                                {{ $orderInfo['shipCode'] }} : {{ $orderInfo['shipName'] }}
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold text-blue">ที่อยู่สถานที่ส่ง</td>
                        <td>
                            @if ($orderInfo['shipName'] != '' || $orderInfo['shipName'] != null)
                                {{ $orderInfo['shipHouseNo'] }} {{ $orderInfo['shipAddress'] }} {{ $orderInfo['shipDistrictName'] }}
                                <br> {{ $orderInfo['shipCityName'] }} {{ $orderInfo['shipPostCode'] }}
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
            <div class="print__client__right" style="height: 223px">
                <table>
                    <tr>
                        <td class="text-bold" width="60">
							เลขที่ใบสั่งซื้อ
                        </td>
                        <td>
                            {{ $orderInfo['documentNumber'] }}
                            <strong> / วันที่ {{ date('d/m/Y', strtotime($orderInfo['documentDate'])) }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold" width="60">
                            PO number
                        </td>
                        <td>
                            {{ ($orderInfo['customerPO'] != '') ? $orderInfo['customerPO'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold">
                            Request Date
                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($orderInfo['requestDate'])) }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold">
							การชำระเงิน
                        </td>
                        <td>
                            {{ ($orderInfo['paymentTerm'] == 'CASH') ? 'เงินสด' : 'เครดิต' }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-bold">
							จัดส่งโดย
                        </td>
                        <td>
                            @if ($orderInfo['shipCondition'] != '01')
                                {{ $orderInfo['transportZone'] }} : {{ $orderInfo['transportZoneDesc'] }}
                            @else
								ลูกค้ารับสินค้าเอง
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="print__order">
            <table>
                <thead>
                <tr>
                    <th align="center">
						ผลิตภัณฑ์
                    </th>
                    <th width="50" align="center">
						จำนวน
                    </th>
                    <th width="80" align="center">
						ราคา / หน่วย <span class="text-red">*</span>
                    </th>
                    <th width="80" align="center">
						จำนวนเงิน (บาท)
                    </th>
                </tr>
                </thead>
                @foreach ($products as $product)
                    <tbody>
                        <tr>
                            <td>
                                @if ($product['isBOM'])
                                    <strong>{{ $product['productCode'] }} {{ $product['productNameTh'] }}</strong>
                                @else
                                    {{ $product['productCode'] }} {{ $product['productNameTh'] }}
                                @endif
                                @if ($product['isFreeGoods'] && $product['productCode'][0] != 'S')
                                    <span style="color: red">(ของแแถม)</span>
                                @endif
                            </td>
                            <td align="center">
                                @if (!$product['isBOM'])
                                    {{ $product['qty'] }} {{ $product['unitNameTh'] }}
                                @endif
                            </td>
                            <td align="right">
                                @if (!$product['isBOM'])
                                    {{ number_format((float)$product['amount'], 2, '.', ',') }}
                                @endif
                            </td>
                            <td align="right">
                                @if (!$product['isBOM'])
                                    {{ number_format((float)$product['totalAmount'], 2, '.', ',') }}
                                @endif
                            </td>
                        </tr>
                        @foreach ($boms as $bom)
                            @if ($bom['productRefCode'] == $product['productCode'])
                                <tr>
                                    <td>
                                        {{ $bom['productCode'] }} {{ $bom['productNameTh'] }}
                                    </td>
                                    <td align="center">
                                        {{ $bom['qty'] }} {{ $bom['unitNameTh'] }}
                                    </td>
                                    <td align="right">
                                        {{ number_format((float)$bom['price'], 2, '.', ',') }}
                                    </td>
                                    <td align="right">
                                        {{ number_format((float)$bom['amount'], 2, '.', ',') }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                @endforeach
                <tfoot>
                    <tr>
                        <th colspan="1" align="center">
							ยอดรวมมูลค่าสินค้า (ยังไม่รวม vat)
                        </th>
                        <th colspan="2" align="center">
                            {{ $totalQty }} รายการ
                        </th>
                        <th align="right">
                            {{ number_format((float)$totalAmount, 2, '.', ',') }}
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="print__note">
            <span class="text-red" style="font-size: 18px;">
				* ราคาต่อหน่วย หลังหักส่วนลดมาตรฐานเท่านั้น
            </span>
        </div>
    </div>
@endsection