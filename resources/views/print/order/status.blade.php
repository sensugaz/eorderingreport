@extends('layout.print')

@section('content')
    <div class="print__container">
        <div class="print__header">
            <div class="print__header__logo">
                <img src="{{ asset('images/toa-logo-print.png') }}">
            </div>
            <div class="print__header__issuer">
                <p>บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จํากัด (มหาชน)</p>
                <p>สำนักงาน และศูนย์อุตสาหกรรม ทีโอเอ บางนา-ตราด 31/2 หมู่ 3</p>
                <p>ถนนบางนา-ตราด ตำบลบางเสาธง อ.บางเสาธง จ.สมุทรปราการ 10570</p>
            </div>
            <div class="print__header__option">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50" align="right" class="text-bold">
							วันที่&nbsp;
                        </td>
                        <td>{{ date('d/m/Y', strtotime($orderPrecessInfo['docDate'])) }}</td>
                    </tr>
                    <tr>
                        <td width="50" align="right" class="text-bold">
							เลขที่&nbsp;
                        </td>
                        <td>{{ $orderPrecessInfo['salesDocument'] }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="print__title">
			สถานะการสั่งซื้อ
        </div>
        <div class="print__client">
            <div class="print__client__full">
                <div class="print__client__full__left">
                    <table>
                        <tr>
                            <td class="text-bold" width="60">
								ชื่อร้านค้า
                            </td>
                            <td>{{ substr($orderPrecessInfo['payerNo'], 2) }} {{ $orderPrecessInfo['payerName'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-bold" width="60">
								เลขใบสั่งซื้อ
                            </td>
                            <td>
								{{ $orderPrecessInfo['woNumber'] }}
								<strong> / วันที่ </strong>
								{{ date('d/m/Y', strtotime($orderPrecessInfo['createDate'])) }}
							</td>
                        </tr>
                        <tr>
                            <td class="text-bold" width="60">
								เลขที่เอกสาร
                            </td>
                            <td>
                                {{ $orderPrecessInfo['salesDocument'] }}
                                <strong> / วันที่ </strong>
                                {{ date('d/m/Y', strtotime($orderPrecessInfo['docDate'])) }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="print__client__full__right">
                    <table>
                        <tr>
                            <td class="text-bold" width="60">
								สถานที่จัดส่ง
                            </td>
                            <td>
                                @if ($orderInfo['shipCode'])
                                    {{ $orderInfo['shipCode'] }} : {{ $orderInfo['shipName'] }}
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold" width="60">
								จัดส่งโดย
                            </td>
                            <td>
                                @if ($orderInfo['shipCondition'] != '01')
                                    {{ $orderInfo['transportZone'] }} : {{ $orderInfo['transportZoneDesc'] }}
                                @else
									ลูกค้ารับสินค้าเอง
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="print__order">
            <table>
                <thead>
                <tr>
                    <th align="center">
						รายการสินค้า
                    </th>
                    <th align="center" width="40">
						สั่งซื้อ
                    </th>
                    <th align="center" width="40">
						ออกบิล
                    </th>
                    <th align="center" width="40">
						การจอง
                    </th>
                    <th align="center" width="40">
						คงค้าง
                    </th>
                    <th align="center" width="40">
						ยกเลิก
                    </th>
                    <th align="center" width="50">
						หน่วย
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>
                            <div style="font-weight: {{ $product['material'][0] == 'S' ? 'bold' : 'normal' }}">
                                {{ $product['material'] }} <br> {{ $product['materialDes'] }}

                                @if ($product['freeGoods'] && $product['material'][0] != 'S')
                                    <span style="color: red">(ของแแถม)</span>
                                @endif
                            </div>
                        </td>
                        <td align="center">
                            @if ($product['material'][0] != 'S')
                                {{ $product['targetQty'] }}
                            @endif
                        </td>
                        <td align="center">
                            @if ($product['material'][0] != 'S')
                                {{ $product['billQty'] }}
                            @endif
                        </td>
                        <td align="center">
                            @if ($product['material'][0] != 'S')
                                {{ $product['deliQty'] }}
                            @endif
                        </td>
                        <td align="center">
                            @if ($product['material'][0] != 'S')
                                {{ $product['balaQty'] }}
                            @endif
                        </td>
                        <td align="center">
                            @if ($product['material'][0] != 'S')
                                {{ $product['rejeQty'] }}
                            @endif
                        </td>
                        <td>
                            @if ($product['material'][0] != 'S')
                                {{ $product['unit'] }}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th align="center">
						รวม
                    </th>
                    <th align="center">
                        {{ $targetQty }}
                    </th>
                    <th align="center">
                        {{ $billQty }}
                    </th>
                    <th align="center">
                        {{ $deliQty }}
                    </th>
                    <th align="center">
                        {{ $balaQty }}
                    </th>
                    <th align="center">
                        {{ $rejeQty }}
                    </th>
                    <th align="center">
                        <!-- EMPTY -->
                    </th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection