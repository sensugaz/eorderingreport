@extends('layout.print')

@section('content')
    <div class="print__container">
        <div class="print__header">
            <div class="print__header__logo">
                <img src="{{ asset('images/toa-logo-print.png') }}">
            </div>
            <div class="print__header__issuer">
                <p>บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จํากัด (มหาชน)</p>
                <p>สำนักงาน และศูนต์อุตสาหกรรม ทีโอเอ บางนา-ตราด 31/2 หมู่ 3 ถนนบางนา-ตราด กม.23</p>
                <p>ต.บางเสาธง อ.บางเสาธง จ.สมุทรปราการ 10540 โทร. 02-335-5555</p>
            </div>
        </div>
        <div class="print__title">
            ตะกร้าสินค้า
        </div>
        <div class="print__client">
            <div class="print__client__full">
                <div class="print__client__full__left">
                    <table>
                        <tr>
                            <td class="text-bold" width="60">
                                ชื่อร้านค้า
                            </td>
                            <td>
                                {{ $products[0]['customerName'] }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="print__order">
            <table>
                <thead>
                    <tr>
                        <th align="center">
                            ผลิตภัณฑ์
                        </th>
                        <th width="50" align="center">
                            จำนวน
                        </th>
                        <th width="80" align="center">
                            ราคา / หน่วย <span class="text-red">*</span>
                        </th>
                        <th width="80" align="center">
                            จำนวนเงิน (บาท)
                        </th>
                    </tr>
                </thead>
                @foreach ($products as $product)
                    <tbody>
                        <tr>
                            <td>
                                @if ($product['isBOM'] == '')
                                    {{ $product['productCode'] }} {{ $product['productNameTh'] }}
                                @else
                                    <strong>{{ $product['productCode'] }} {{ $product['productNameTh'] }}</strong>
                                @endif
                            </td>
                            <td align="center">
                                @if ($product['isBOM'] == '')
                                    {{ $product['qty'] }} {{ $product['unitNameTh'] }}
                                @endif
                            </td>
                            <td align="right">
                                @if (!$product['isBOM'])
                                    {{ number_format((float)$product['amount'], 2, '.', ',') }}
                                @endif
                            </td>
                            <td align="right">
                                @if (!$product['isBOM'])
                                    {{ number_format((float)$product['totalAmount'], 2, '.', ',') }}
                                @endif
                            </td>
                        </tr>
                        @foreach ($boms as $bom)
                            @if ($bom['productRefCode'] == $product['productCode'])
                                <tr>
                                    <td>
                                        {{ $bom['productCode'] }} {{ $bom['productNameTh'] }}
                                    </td>
                                    <td align="center">
                                        {{ $product['qty'] }} {{ $bom['unitNameTh'] }}
                                    </td>
                                    <td align="right">
                                        {{ number_format((float)$bom['price'], 2, '.', ',') }}
                                    </td>
                                    <td align="right">
                                        {{ number_format((float)$bom['price'] * $product['qty'], 2, '.', ',') }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                @endforeach
                <tfoot>
                    <tr>
                        <th colspan="1" align="center">
                            ยอดรวมมูลค่าสินค้า (ยังไม่รวม vat)
                        </th>
                        <th colspan="2" align="center">
                            {{ $totalQty }} รายการ
                        </th>
                        <th align="right">
                            {{ number_format((float)$totalAmount, 2, '.', ',') }}
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection