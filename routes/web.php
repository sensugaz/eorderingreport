<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'eOrdering Sysmtem';
});

Route::get('/print/cart', 'PrintController@cart');

Route::get('/print/order/invoice', 'PrintController@orderInvoice');
Route::get('/print/order/detail', 'PrintController@orderDetail');
Route::get('/print/order/status', 'PrintController@orderStatus');
Route::get('/print/order/billing', 'PrintController@orderBilling');