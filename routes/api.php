<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'report'], function() {
    Route::get('monthly', 'ReportController@monthly');
    Route::get('quarter', 'ReportController@quarter');
	Route::get('salesman', 'ReportController@salesman');
	Route::get('manager', 'ReportController@manager');
	
    Route::get('monthly/{fileName}', 'ReportController@getMonthly');
    Route::get('quarter/{fileName}', 'ReportController@getQuarter');
	Route::get('salesman/{fileName}', 'ReportController@getSalesman');
	Route::get('manager/{fileName}', 'ReportController@getManager');
});